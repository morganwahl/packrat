all: requirements.txt dist/%

clean:
	rm -rf .tox
	python setup.py clean --all

requirements.txt: setup.py
	pip-compile > requirements.txt

dist/%: setup.py
	python setup.py sdist
	python setup.py bdist_wheel
	twine upload dist/*
